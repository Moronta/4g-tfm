#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()

struct sockaddr_in serv_addr, client;
int fd, baudrate=B115200, sock;
/* ****************************************
 * Function name: setup()
 * 
 * Description:
 * 		Open uart communication and configure
 * 		the socket client
 * 
 *************************************** */
int setup(){
	
	int opt=1;
	sock=0;
	
	/* Start UART communication */
	if((fd=open("/dev/ttyUSB0",O_RDWR))<0) return 1;
	/* *** Configure Port *** */
	struct termios tty;
	memset (&tty, 0, sizeof tty);

	/* Error Handling */
	if ( tcgetattr ( fd, &tty ) != 0 )
	{
		printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
	}


	tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
	tty.c_cflag |= CS8; // 8 bits per byte (most common)
	tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
	tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)
	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~ECHO; // Disable echo
	tty.c_lflag &= ~ECHOE; // Disable erasure
	tty.c_lflag &= ~ECHONL; // Disable new-line echo
	tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed


	tty.c_cc[VTIME] = 0;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
	tty.c_cc[VMIN] = 0;

	// Set in/out baud rate to be 9600
	cfsetispeed(&tty, baudrate);
	cfsetospeed(&tty, baudrate);
	// Save tty settings, also checking for error
	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
    		printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
	}

	printf("Serial communication opened \n");
	fflush(stdout);
	
	/* Listening socket */
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
	{ 
		printf("\n Socket creation error \n"); 
		return -1; 
	} 
	/* Allow address reuse on socket */
	/*if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR , &opt, sizeof(opt)))
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}*/
	/* Address, port and socket protocol */
	memset(&client, '0', sizeof(client));
	client.sin_addr.s_addr = inet_addr("127.0.0.1");
	client.sin_family = AF_INET;
	client.sin_port = htons(1881);
	

	//Connect to remote server
	if (connect(sock , (struct sockaddr *)&client , sizeof(client)) < 0)
	{
		perror("connect failed. Error");
		return 1;
	}
	printf("Connected \n");


	return 0;
}


/* ****************************************
 * Function name: PI_THREAD(socketRead)
 * 
 * Description:
 * 		Read localhost port and send data
 * 		to uart connection
 * 
 *************************************** */
void *thread_socket(){

	int valread;
	char buffer[10096]={0}; 

	/* Reading all the time */
	for(;;){
		memset(buffer,0,sizeof(buffer));
		if((valread = read( sock, buffer, sizeof(buffer)))>0){
			printf("Received data from Broker, sending to xbee..%i\n",valread);
			/* Sending data to uart connection */
			write(fd,buffer,valread);
		}
		else{
			printf("Socket error\n");
			/* Listening socket */
			if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
			{ 
				printf("\n Socket creation error \n"); 
				return -1; 
			} 
			/* Address, port and socket protocol */
			memset(&client, '0', sizeof(client));
			client.sin_addr.s_addr = inet_addr("127.0.0.1");
			client.sin_family = AF_INET;
			client.sin_port = htons(1881);
	

			//Connect to remote server
			if (connect(sock , (struct sockaddr *)&client , sizeof(client)) < 0)
			{
				perror("connect failed. Error");
				return 1;
			}
			printf("Connected \n");
		}

	} 
}



/* ****************************************
 * Function name: PI_THREAD(uartRead)
 * 
 * Description:
 * 		Read uart port and send data
 * 		to Broker
 * 
 *************************************** */
void *thread_uart(){
	

	int valread;
	char buffer[10096]={0}; 

	
	/* Reading all the time */
	for(;;){
		memset(buffer,0,sizeof(buffer));
		if((valread = read( fd, buffer, sizeof(buffer)))>0){
			printf("Uart data received..\n");
			/* Sending data to localhost */
			if((write( sock, buffer,valread))<=0){
				printf("Error writting\n");
			}
			
		}
	} 
	
}



/* ****************************************
 * Function name: main()
 * 
 * Description:
 * 		Create both threads and call
 * 		setup function
 * 
 *************************************** */
int main(){
	
	setup();
	/* Thread creation */
   	pthread_t h1;
	pthread_t h2;
	pthread_create(&h1,NULL,thread_socket,NULL);
   	pthread_create(&h2,NULL,thread_uart,NULL);
	for(;;){}
	
	return 0;
}
